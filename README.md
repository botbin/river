# river
river uses the WebSocket protocol to exchange data with the web clients
in real time.

## Authentication
Clients must send a JWT along with their upgrade request. It must be
stored in the `token` query parameter.

## Environment
- JWT_AUTH
  - This is the secret used to verify access tokens during the initial
    upgrade handshake with clients. If not provided, a random secret
    will be generated.
- REDIS_HOST
  - The hostname for the Redis instance