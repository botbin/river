import ModuleChangeHandler from "./module";
import { store as progressStore } from './progress-store';
import Client from '../client/client';

/** Manages collections of module works in progress */
export default class ModuleService {
    private changeHandler: ModuleChangeHandler;
    private validName: RegExp;

    constructor() {
        this.changeHandler = new ModuleChangeHandler();
        this.validName = /^[a-z0-9](-?[a-z0-9])*$/;
    }

    /** Enables the client to interact with the service over WebSocket */
    register(client: Client) {
        client.socket.on('request.module.content', (name) => {
            if (name.length < 21 && this.validName.test(name)) {
                this.sendWork(client, name);
            }
            // TODO: Let them know if the name is invalid.
        });

        client.socket.on('request.module.list', () => this.giveList(client));
    }

    /**
     * Loads the work in progress for a module into the client/server
     * communication channel. After sending the work, any client-side
     * events regarding its modification will be detected here.
     */
    sendWork(client: Client, moduleName: string) {
        progressStore.get(client.id, moduleName, (err, wip) => {
            if (!err && wip) {
                progressStore.setCurrentModule(client.id, moduleName);
                this.changeHandler.register(client, wip);
                client.socket.emit('response.module.content', wip.marshal());
            }
        });
    }

    /** Gives the client a list of their module names */
    giveList(client: Client) {
        progressStore.getModuleNames(client.id, names => {
            client.socket.emit('response.module.list', names);
        });
    }
}