import log from '../../log/logger';
import { store } from './progress-store';
import WorkInProgress from './work-in-progress';
import Client from '../client/client';

/**
 * Responds to changes in modules by storing their new state
 * 
 * ModuleChangeHandler connects socket events that signal partial
 * module changes to methods that store those changes in a database.
 */
export default class ModuleChangeHandler {
    private handlers: {[k: string]: (wip: WorkInProgress, v: string) => void}

    constructor() {
        this.handlers = {};

        this.handlers[this.codeKey()] = (wip, code) => {
            wip.code = code;
            store.saveCode(wip);
        };
        this.handlers[this.descriptionKey()] = (wip, desc) => {
            wip.description = desc;
            store.saveDescription(wip);
        };
    }

    /** Returns the name of the event for code changes */
    codeKey(): string {
        return 'module.code.changed';
    }

    /** Returns the name of the event for description changes */
    descriptionKey(): string {
        return 'module.description.changed';
    }

    /**
     * Registers a client to have information about its module modifications stored
     * @param client - A client that will modify a module
     * @param wip - The location to store module changes
     */
    register(client: Client, wip: WorkInProgress) {
        log.info('loading module autosave context', {
            user_id: client.id,
            module_name: wip.name
        });

        Object.keys(this.handlers).forEach(key => {
            client.socket.removeAllListeners(key);
            client.socket.on(key, data => this.handlers[key](wip, data));
        });
    }
}
