import redis from '../../data/redis';
import WorkInProgress from './work-in-progress';
import log from '../../log/logger';

/** Maintains a persistent store of all works in progress */
export class ProgressStore {
    private makeWipKey(userId: string, moduleName: string): string {
        return 'module_wip:' + userId + ':' + moduleName;
    }

    /** Saves the code of a work in progress */
    saveCode(wip: WorkInProgress) {
        const key = this.makeWipKey(wip.worker, wip.name);
        redis.hset(key, 'code', wip.code);
    }

    /** Saves the description of a work in progress */
    saveDescription(wip: WorkInProgress) {
        const key = this.makeWipKey(wip.worker, wip.name);
        redis.hset(key, 'description', wip.description);
    }

    /** Gets the user's current work in progress */
    getCurrentWork(userId: string, cb: (e: Error | null, wip?: WorkInProgress) => void) {
        redis.hget('current_wip', userId, (err, name) => {
            if (err) {
                this.logGetFailure(userId, '', err);
                cb(err);
            } else {
                this.get(userId, name, cb);
            }
        });
    } 

    /** Gets the work in progress for a module */
    get(userId: string, moduleName: string, cb: (e: Error | null, wip?: WorkInProgress) => void) {
        const key = this.makeWipKey(userId, moduleName);

        redis.hgetall(key, (err, res) => {
            if (err) {
                this.logGetFailure(userId, moduleName, err);
                cb(err);
            } else if (!res) {
                cb(null, new WorkInProgress(userId, moduleName, '', ''));
            } else {
                cb(null, new WorkInProgress(userId, moduleName, res.code, res.description));
            }
        });
    }

    private logGetFailure(userId: string, moduleName: string, err: Error) {
        log.warn('failed to get work in progress', {
            user_id: userId,
            module: moduleName,
            error: err
        });
    }

    /** Sets the name of the module currently being worked on */
    setCurrentModule(userId: string, moduleName: string) {
        redis.batch()
            .hset('current_wip', userId, moduleName)
            .sadd('wips:' + userId, moduleName)
            .exec((err, res) => log.error('failed to set current module', {
                user_id: userId,
                module: moduleName,
                err: err
            }));
    }

    /** Gets the names of each module the user has worked on */
    getModuleNames(userId: string, cb: (names: string[]) => void) {
        redis.smembers('wips:' + userId, (err, names) => {
            if (err) {
                log.error('failed to fetch module names', {
                    user_id: userId,
                    error: err
                });
                cb([]);
            } else {
                cb(names);
            }
        });
    }
}

export let store = new ProgressStore();