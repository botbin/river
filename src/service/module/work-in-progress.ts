/**
 * Defines an incomplete module that will receive many partial edits
 */
export default class WorkInProgress {
    /**
     * 
     * @param worker The id of the user who is working
     * @param name The name of the module this is for
     * @param code The code for this work
     * @param description The description for this work
     */
    constructor(
        public worker: string,
        public name: string,
        public code: string,
        public description: string
    ) {}

    /** Returns a string representation of the work */
    marshal(): string {
        return JSON.stringify({
            name: this.name,
            code: this.code,
            description: this.description
        });
    }
}