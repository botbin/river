import socketio from 'socket.io';
import Identity from './identity';

/** Models a WebSocket connection to a web client */
export default class Client {
    /** A unique ID for this client */
    readonly id: string;

    /**
     * @param socket The underlying websocket connection
     * @param identity The JWT claims of the client's access  token
     */
    constructor(
        readonly socket: socketio.Socket,
        readonly identity: Identity
    ) {
        this.id = this.identity.sub;
    }
}