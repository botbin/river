import Client from './client';
import ModuleService from '../module/service';
import NotificationService from '../notify/service';
import log from '../../log/logger';
import socketio from 'socket.io';
import Identity from './identity';

/** Manages connections to client devices */
export default class ClientService {
    private clientsById: {[id: string]: Client};
    private clientCount: number;
    private moduleService: ModuleService;
    private notificationService: NotificationService;

    constructor() {
        this.clientsById = {};
        this.clientCount = 0;
        this.moduleService = new ModuleService();
        this.notificationService = new NotificationService();
    }

    /**
     * Registers a client socketio connection in the system
     * 
     * @param socket A socket.io socket
     * @param identity JWT claims regarding a socket owner
     */
    register(socket: socketio.Socket, identity: Identity) {
        let client = this.storeClient(socket, identity);

        this.moduleService.register(client);
        this.notificationService.register(client);

        this.handleClientDisconnect(client);
    }

    private storeClient(socket: socketio.Socket, identity: Identity) {
        let client = new Client(socket, identity);
        this.clientsById[client.id] = client;
        this.clientCount++;

        log.info('client connected', { user_id: client.id });
        return client;
    }

    /** Takes care of removing client resources when they disconnect */
    private handleClientDisconnect(client: Client) {
        client.socket.on('disconnect', () => {
            this.notificationService.remove(client);
            delete this.clientsById[client.id];
            this.clientCount--;            

            log.info('client disconnected', { user_id: client.id });
        });
    }

    /** Returns the number of registered clients */
    count(): number {
        return this.clientCount;
    }
}