/** Identity specifies common properties of authenticated users */
export default interface Identity {
    /** The globally-unique identifier */
    readonly sub: string;

    /**
     * The self-assigned username
     * 
     * This should be unique, but prefer using 'sub' to identify
     * users instead of this; it may not be unique in the future.
     */
    readonly username: string;
}