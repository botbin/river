import NotificationHandler from './notify';
import redis from '../../data/redis';
import Client from '../client/client';
import RedisMessageSource from './redis';

/** Registers a client to receive live notifications to its socket */
export default class NotificationService {
    private readonly handler: NotificationHandler;

    constructor() {
        const source = new RedisMessageSource(redis);
        this.handler = new NotificationHandler(source);
    }
    
    /** Registers the client to receive notifications as they come in */
    register(client: Client) {
        this.handler.accept(client.identity.username, client.socket);
    }

    /** Stops sending incoming notifications to the client */
    remove(client: Client) {
        this.handler.remove(client.identity.username);
    }
}