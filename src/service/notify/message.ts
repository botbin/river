/** Models a stream of categorized messages */
export interface MessageSource {
    /**
     * Subscribes a message handler to a stream of messages
     * of some category
     * 
     * @param topic The category of message to receive
     * @param consume Called with each received message
     * @returns A method that, when called, will unsubscribe from the topic
     */
    subscribe(topic: string, consume: MessageHandler): SubCanceler;
}

/** Processes a message from a MessageSource */
export type MessageHandler = (message: string) => void;
/** Cancels a subscription to a MessageSource */
export type SubCanceler = () => void;