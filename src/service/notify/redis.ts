import { MessageHandler, SubCanceler, MessageSource } from './message';
import { RedisClient } from 'redis';

/** Implements a MessageSource using Redis pub/sub functionality */
export default class RedisMessageSource implements MessageSource {
    constructor(private client: RedisClient) {}

    /**
     * Attaches a message handler to all messages received from
     * a Redis channel. Messages are received until the cancel
     * method is called.
     * 
     * @param channel The channel to consume messages from
     * @param consume The method that will process each message
     * @returns A method that, when called, ends the channel subscription
     */
    subscribe(channel: string, consume: MessageHandler): SubCanceler {
        const subscriber = this.client.duplicate();
        subscriber.on('message', (channel, message) => consume(message));
        subscriber.subscribe(channel);
        return this.createCanceler(subscriber);
    }

    private createCanceler(subscriber: RedisClient): SubCanceler {
        return () => {
            subscriber.unsubscribe();
            subscriber.quit();
        };
    }
}