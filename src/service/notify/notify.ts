import EventHandler from "../../web/handler";
import { EventEmitter } from "events";
import { SubCanceler, MessageSource } from "./message";

/**
 * Handles client notifications
 * 
 * The NotificationHandler consumes client notifications
 * from a message source and emits them to a relevant
 * destination.
 */
export default class NotificationHandler implements EventHandler {
    private topicCancelations: {[topic: string]: SubCanceler};

    /** The key used when emitting notifications */
    readonly eventKey: string;

    /**
     * @param source The source of all notifications
     */
    constructor(private readonly source: MessageSource) {
        this.topicCancelations = {};
        this.eventKey = 'notification';
    }

    /**
     * Accepts future notifications for the user until a call to remove is made
     * 
     * Notifications are emitted to the emitter under the eventKey key.
     * All notifications are objects encoded as strings.
     * 
     * @param username The username to receive notifications for
     * @param emitter An emitter belonging to the user
     */
    accept(username: string, emitter: EventEmitter) {
        const topic = this.getTopic(username);
        const unsubscribe = this.source.subscribe(topic, message => {
            const notification = JSON.parse(message);
            emitter.emit(this.eventKey, notification);
        });

        this.topicCancelations[topic] = unsubscribe;
    }

    /**
     * Stops sending notifications to a user
     * 
     * It is safe to call this method if you are unsure if
     * they are accepting notifications.
     */
    remove(username: string) {
        const topic = this.getTopic(username);

        let unsubscribe = this.topicCancelations[topic];
        if (unsubscribe) {
            unsubscribe();
            delete this.topicCancelations[topic];
        }
    }

    private getTopic(username: string): string {
        return 'notification_stream:' + username;
    }
}