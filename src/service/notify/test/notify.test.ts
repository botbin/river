import { MessageSource, SubCanceler, MessageHandler } from '../message';
import NotificationHandler from '../notify';
import { EventEmitter } from 'events';

describe('NotificationHandler', () => {
    it('Should route notifications from a MessageSource to EventEmitter', () => {
        let sourcePublisher: MessageHandler = m => {};
        let unsubscribed = false;
        const source: MessageSource = {
            subscribe(topic: string, handler: MessageHandler): SubCanceler {
                sourcePublisher = handler;
                return () => { unsubscribed = true };
            }
        };
    
        const handler = new NotificationHandler(source); 
    
        const username = 'testUser'
        const userInbox = new EventEmitter();
        let gotMessage = false;
        userInbox.on(handler.eventKey, () => { gotMessage = true; });
    
        handler.accept(username, userInbox);
    
        sourcePublisher(JSON.stringify({msg: 'test'}));
        expect(gotMessage).toBe(true);
    
        handler.remove(username);
        handler.remove(username); // Repeat calls shouldn't fail.
        expect(unsubscribed).toBe(true);
    });
});