import socketio from 'socket.io';
import log from '../../log/logger';
import http from 'http';
import ClientService from '../../service/client/service';
import Authenticator from './authenticator';

/**
 * Connects valid WebSocket connections to internal services
 * 
 * WebSocket is the protocol that this service uses to communicate
 * with external clients. Those clients must provide an authentic
 * JWT along with their connection request in order to gain access.
 */
export default class WebSocketServer {
    private readonly server: http.Server;
    private readonly io: socketio.Server;
    private readonly auth: Authenticator;
    private readonly clientService: ClientService;

    /**
     * Constructs a new WebSocketServer instance
     * 
     * This will not start the server. That can be done by calling listen.
     * @param server A configured HTTP server
     * @param authSecret The secret used to verify access tokens
     */
    constructor(server: http.Server, authSecret: string) {
        this.server = server;
        this.auth = new Authenticator(authSecret);
        this.clientService = new ClientService();

        this.io = socketio();
        this.io.on('connection', this.onNewConnection.bind(this));
    }

    /** Begins listening for new WebSocket connections */
    listen() {
        this.io.listen(this.server);
    }

    /** Stops listening for new connections */
    close() {
        this.io.close();
    }

    /** Processes a socket connection that has passed the upgrade stage */
    private onNewConnection(socket: socketio.Socket) {
        try {
            const token = socket.handshake.query.token;
            const identity = this.auth.authenticate(token);
            this.clientService.register(socket, identity);
        } catch (err) {
            const remoteAddr = socket.request.connection.remoteAddress;
            log.info('rejected new client connection', {
                remoteAddr: remoteAddr,
                error: err
            });
            socket.disconnect();
        }
    }
}