import verifier from 'jsonwebtoken';
import Identity from '../../service/client/identity';

/** Verifies the authenticity of JWTs */
export default class Authenticator {
    constructor(private readonly secret: string) {}

    /** 
     * Authenticates a JSON Web Token
     * 
     * @throws Error if the token is not properly signed or is missing
     *         claims required to form an Identity
     */
    authenticate(jwt: string): Identity {
        const decoded = verifier.verify(jwt, this.secret) as {[k:string]:any};
        if (!decoded.sub) throw new Error('missing sub claim');
        if (!decoded.username) throw new Error('missing username claim');
        return {
            sub: decoded.sub,
            username: decoded.username
        };
    }
}