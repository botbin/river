import log from '../../log/logger';
import HttpServer from './http';
import WebSocketServer from './ws';
import http from 'http';

/** Manages the protected WebSocket entry point */
export default class Server {
    private httpServ: HttpServer;
    private ws: WebSocketServer;
    
    /**
     * Constructs a new Server instance
     * This will not start the server. That can be done by calling listen.
     * @param authSecret The secret used to verify access tokens
     */
    constructor(authSecret: string) {
        this.httpServ = new HttpServer();
        this.ws = new WebSocketServer(this.httpServ.server, authSecret);
    }

    /** Opens the server up to connections */
    listen(port: number) {
        this.httpServ.listen(port);
        this.ws.listen();
        log.info('server started', { port: port });
    }

    /** Stops listening for new connections and closes those that are open */
    stopListening() {
        this.ws.close();
        this.httpServ.close();
    }

    /** Returns the raw HTTP server */
    raw(): http.Server {
        return this.httpServ.server;
    }
}