import crypto from 'crypto';
import * as jwt from 'jsonwebtoken';
import Authenticator from '../authenticator';

describe('Authenticator', () => {
    it('should throw an error if username is missing from claims', () => {
        let keyToken = makeToken({sub: 3}, false);
        let auth = new Authenticator(keyToken[0]);
        expect(() => {
            auth.authenticate(keyToken[1]);
        }).toThrow();
    });

    it('should throw an error if sub is missing from claims', () => {
        let keyToken = makeToken({username: 'test'}, false);
        let auth = new Authenticator(keyToken[0]);
        expect(() => {
            auth.authenticate(keyToken[1]);
        }).toThrow();
    });

    it('should throw an error if the token has expired', () => {
        const claims = {
            username: 'test',
            sub: 3
        };
        let keyToken = makeToken(claims, true);

        let auth = new Authenticator(keyToken[0]);
        expect(() => {
            auth.authenticate(keyToken[1]);
        }).toThrow();
    });

    it('should throw an error if the signature is invalid', () => {
        const now = Math.floor(Date.now() / 1000);
        const claims = {
            exp: now + 100,
            username: 'test',
            sub: 3
        };
        const key = crypto.randomBytes(32).toString('hex');
        const token = jwt.sign(claims, key);

        const auth = new Authenticator(crypto.randomBytes(32).toString('hex'));
        expect(() => {
            auth.authenticate(token);
        }).toThrow();
    });

    it('should create an identity for valid tokens', () => {
        const claims = {
            username: 'test',
            sub: 3
        };
        let keyToken = makeToken(claims, false);

        let auth = new Authenticator(keyToken[0]);
        expect(auth.authenticate(keyToken[1])).toBeTruthy();
    });
});

// returns (signing key, token)
function makeToken(claims: {[k:string]:any}, expired: boolean): [string, string] {
    const now = Math.floor(Date.now() / 1000);
    claims.exp = expired ? now - 1 : now + 100;

    const key = crypto.randomBytes(32).toString('hex');    
    const token = jwt.sign(claims, key);
    return [key, token];
}