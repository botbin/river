import http from 'http';

/** Creates a server that handles requests for HTTP resources */
export default class HttpServer {
    server: http.Server;

    constructor() {
        this.server = http.createServer(this.handleRequest);
    }
    
    private handleRequest(req: http.IncomingMessage, res: http.ServerResponse) {
        if (req.url === '/healthz') {
            res.writeHead(200);
        } else {
            res.writeHead(404);
        }
        res.end();
    }

    /** Begins listening for new requests to a specified port */
    listen(port: number) {
        this.server.listen(port);
    }

    /** Stops listening for new requests */
    close() {
        this.server.close();
    }
}