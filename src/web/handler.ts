import { EventEmitter } from "events";

/**
 * Specifies a handler that responds to events from either
 * the service or the emitter.
 * 
 * [external source] <-> [emitter] <-> EventHandler <-> [internal service]
 */
export default interface EventHandler {
    /**
     * Registers an emitter for future event processing
     * @param key A unique key to identify the emitter
     */
    accept(key: string, emitter: EventEmitter): void;

    /**
     * Stops handling events for the emitter with this key
     * @param key The key given when accept was called
     */
    remove(key: string): void;
}