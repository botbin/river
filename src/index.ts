import crypto from 'crypto';
import log from './log/logger';
import Server from './web/server/server';

const pkg = require('../package.json');
log.info(`${pkg.name} v${pkg.version}`);

let secret = process.env.JWT_AUTH;
if (!secret || secret === '') {
    log.error('missing JWT secret; one will be randomly generated');
    secret = crypto.randomBytes(32).toString('hex');
}
new Server(secret).listen(8000);
