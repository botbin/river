import log from '../log/logger';
import redis from 'redis';

const redisHost = process.env.REDIS_HOST;
const client: redis.RedisClient = redis.createClient({ host: redisHost });

client.on('error', (err: Error) => {
    log.error('encountered Redis error', { error: err });
});

client.on('connect', () => {
    log.info('connected to Redis server', { redis_host: redisHost });
});

export default client;