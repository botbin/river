import winston from 'winston';
const log = winston.createLogger({
    transports: [
        new winston.transports.Console({
            format: winston.format.json()
        })
    ]
});

export default log;