FROM node:9.11-alpine

RUN apk --no-cache add curl \
 && npm install -g typescript

WORKDIR /var/lib/river
COPY . .

RUN npm install  \
 && tsc          \
 && rm -rf ./src

HEALTHCHECK --interval=3s --timeout=5s --start-period=5s --retries=3 \
            CMD curl --fail http://localhost:8000/healthz || exit 1

ENV NODE_ENV production
EXPOSE 8000

CMD [ "npm", "start" ]